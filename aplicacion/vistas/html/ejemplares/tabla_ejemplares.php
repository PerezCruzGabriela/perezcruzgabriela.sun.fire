
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<caption>Ejemplares</caption>
			<thead>
				<tr>
					<th>id_ejemplar</th>
					<th>Ejemplar</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
  <?php foreach ($datos['ejemplares'] as $ejemplar) { ?>
    <tr>
					<td><?php echo $ejemplar['id_ejemplar']; ?></td>
					<td><?php echo $ejemplar['observaciones_ejemplar']; ?></td>
					<td><a
						href="inicio_biblioteca.php?c=ejemplares&a=ver_ejemplar&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-primary btn-xs">Información</a> <a
						href="inicio_biblioteca.php?c=ejemplares&a=editar_ejemplar&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-default btn-xs">Editar</a> <a
						href="inicio_biblioteca.php?c=ejemplares&a=borrar_ejemplar&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>"
						class="btn btn-warning btn-xs">Borrar</a></td>
				</tr>
  <?php } ?>
    </tbody>
		</table>
	</div>
</div>
<?php

/* Modelo */
function obtener_ejemplares($aplicacion, $isbn)
{
     $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    /* Requerimos de acceso a la base de datos */
    require_once "aplicacion/librerias/bd/querys_ejemplares.php";
    /* Y ejecutar algún query en ella */
    return select_ejemplares($isbn);
}

function obtener_ejemplar($aplicacion, $id_ejemplar)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    /*
     * VALIDACIONES QUE NO REQUIEREN DE ACCESO A LA BASE DE DATOS Si el id del ejemplar que el controlador nos está solicitando no es númerico o excede una longitud de 20 caracteres no debemos perder nuestro tiempo ejecutando una consulta en la base de datos.
     */
    if (! is_numeric($id_ejemplar) || strlen($id_ejemplar) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El ejemplar no existe.';
    }
    
    /*
     * En otras "acciones" del modelo (distintas a las "acciones" del controlador) las validaciones que no requieren acceso a la base de datos serán más.
     */
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    /* Finalmente, realizamos la "consulta" a la base de datos */
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    return select_ejemplar($id_ejemplar);
}

function eliminar_ejemplar($aplicacion, $id_ejemplar)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    /*
     * VALIDACIONES QUE NO REQUIEREN DE ACCESO A LA BASE DE DATOS Si el id del ejemplar que el controlador nos está solicitando no es númerico o excede una longitud de 20 caracteres no debemos perder nuestro tiempo ejecutando una consulta en la base de datos.
     */
    if (! is_numeric($id_ejemplar) || strlen($id_ejemplar) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El ejemplar no existe.';
    }
    
    /*
     * En otras "acciones" del modelo (distintas a las "acciones" del controlador) las validaciones que no requieren acceso a la base de datos serán más.
     */
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    /* Finalmente, realizamos la "consulta" a la base de datos */
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    return delete_ejemplar($id_ejemplar);
}

function guardar_datos_ejemplar($aplicacion, $ejemplar)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    if (empty($ejemplar['observaciones_ejemplar'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el nombre del ejemplar.';
    }
    
    /* ¡¡¡Observa el schema de la base de datos!!! */
    if (empty($ejemplar['isbn'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado la nacionalidad del ejemplar.';
    }
    
    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    return insert_ejemplar($ejemplar);
}

function modificar_datos_ejemplar($aplicacion, $id_ejemplar, $ejemplar)
{
    $resultado = array(
        'error' => false,
        'mensajes_error' => array()
    );
    
    if (! is_numeric($id_ejemplar) || strlen($id_ejemplar) > 20) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'El ejemplar no existe.';
    }
    
    if (empty($ejemplar['observaciones_ejemplar'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado el nombre del ejemplar.';
    }
    
    /* ¡¡¡Observa el schema de la base de datos!!! */
    if (empty($ejemplar['isbn'])) {
        $resultado['error'] = true;
        $resultado['mensajes_error'][] = 'No se ha indicado la nacionalidad del ejemplar.';
    }

    if ($resultado['error'] == true) {
        return $resultado;
    }
    
    require_once "aplicacion/librerias/bd/querys_ejemplar.php";
    return update_ejemplar($id_ejemplar, $ejemplar);
}